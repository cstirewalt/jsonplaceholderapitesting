# JSONPlaceholderAPITesting

Basic API testing demo with a Postman collection.

Instructions on importing the collection into Postman can be found here: https://learning.postman.com/docs/postman/collections/importing-and-exporting-data/#importing-postman-data

Testing against publicly available API documented at: https://jsonplaceholder.typicode.com/ with a focus on the /posts endpoint. 
The collection covers response status codes, response schema validation, extraneous request parameters, invalid input parameters, response time, and partial testing of response headers.

I recommend using a delay of at least 1000ms between tests when executing the collection as the JSONPlaceholder API seems to have issues with multiple consecutive requests.